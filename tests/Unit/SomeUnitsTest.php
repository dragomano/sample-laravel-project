<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use App\Home;

class SomeUnitsTest extends TestCase
{
    /**
     * Простейший тест методов запроса данных
     *
     * @return void
     */
    public function testAccessor()
    {
        // Вытаскиваем данные о главной странице с помощью класса DB
        $db_home = DB::select('Select * From pages Where alias = :alias', ['alias' => '/']);

        // Подготавливаем ожидаемый результат
        $db_home_title = $db_home[0]->title;

        // Загружаем те же самые данные с помощью модели Eloquent
        $model_home = Home::whereAlias('/')->get()[0];
        $model_home_title = $model_home->title;

        // Используем метод assertEquals для сравнения обеих полученных переменных
        $this->assertEquals($db_home_title, $model_home_title);
    }

    /**
     * Проверяем доступность главной страницы
     *
     * @return void
     */
    public function testHomeAreaStatus()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    /**
     * Проверяем доступность страницы «Новости»
     *
     * @return void
     */
    public function testNewsAreaStatus()
    {
        $response = $this->call('GET', 'news');
        $response->assertStatus(200);
    }

    /**
     * Проверяем доступность страницы «Галерея»
     *
     * @return void
     */
    public function testGalleryAreaStatus()
    {
        $response = $this->call('GET', 'gallery');
        $response->assertStatus(200);
    }

    /**
     * Проверяем соответствие используемого метода контроллера главной страницы (см. routes/web.php)
     *
     * @return void
     */
    public function testCurrentAction()
    {
        $response = $this->get('/');
        $view = $response->original;
        $this->assertEquals('App\Http\Controllers\HomeController::index', $view['action']);
    }
}