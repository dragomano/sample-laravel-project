<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\News;
use Faker\Factory as Faker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class SomeFeaturesTest extends TestCase
{
    /**
     * Проверяем добавление новости в базу данных
     *
     * @return void
     */
    public function testAddNews()
    {
        $news = new News;

        $faker = Faker::create();
        $title = $faker->sentence(rand(3, 8), true);
        $title = substr($title, 0, strlen($title) - 1);

        $news->title = $title;
        $news->body  = $faker->realText(rand(200, 300));

        $result = $news->save();
        $this->assertTrue($result);
    }

    /**
     * Проверяем доступность последней добавленной новости
     *
     * @return void
     */
    public function testLastNewsStatus()
    {
        $news = News::latest()->first();
        $response = $this->call('GET', 'news', ['id' => $news->id]);
        $response->assertStatus(200);
    }

    /**
     * Проверяем загрузку файлов в галерею
     *
     * @return void
     */
    public function testUploadFileToGallery()
    {
        Storage::fake('public');

        $file = UploadedFile::fake()->image('sample.png', 1000, 800)->size(200);

        $this->post('gallery', [
            'image[]' => [$file]
        ]);

        $url = url('storage/' . $file->hashName());
        $urlHeaders = @get_headers($url);

        $this->assertTrue(strpos($urlHeaders[0], '200') !== false);
    }
}
