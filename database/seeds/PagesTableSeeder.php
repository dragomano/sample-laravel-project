<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pages = [];

        $current_time = date('Y-m-d H:i:s', time());

        $pages[] = [
            'title'       => 'Демонстрационный проект',
            'alias'       => '/',
            'description' => 'Пример простого сайта на Laravel',
            'body'        => 'Consectetur incididunt adipisicing tempor sint deserunt anim. Non occaecat ad irure ea est reprehenderit proident proident sunt consequat mollit adipisicing. Ipsum aliqua adipisicing aliquip labore et adipisicing labore voluptate.',
            'created_at'  => $current_time,
            'updated_at'  => $current_time
        ];

        DB::table('pages')->insert($pages);
    }
}