<?php

use Faker\Generator as Faker;

/**
 * Генерация случайной новости с помощью библиотеки Faker
 */

$factory->define(\App\News::class, function (Faker $faker) {
    $title      = $faker->sentence(rand(3, 8), true);
    $title      = substr($title, 0, strlen($title) - 1);
    $newsBody   = $faker->realText(rand(200, 300));
    $randomDate = $faker->dateTimeBetween('-3 months', '-2 days');

    $data = [
        'title'      => $title,
        'body'       => $newsBody,
        'created_at' => $randomDate,
        'updated_at' => $randomDate
    ];

    return $data;
});
