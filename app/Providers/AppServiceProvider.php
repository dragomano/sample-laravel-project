<?php

namespace App\Providers;

use Faker\Factory;
use Faker\Generator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Передаем переменную copyright в каждый шаблон при подключении layouts.footer
        View::composer('layouts.footer', function($view) {
            $view->with('copyright', '(C) 2019, Bugo');
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Локализуем текст новостей, генерируемых с помощью фабрики database/factories/NewsFactory
        $this->app->singleton(Generator::class, function () {
            return Factory::create('ru_RU');
        });
    }
}
