<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class NewsController
 *
 * @package App\Http\Controllers
 */
class NewsController extends Controller
{
    /**
     * Отображение всех новостей из таблицы news в базе данных
     *
     * @return Factory|View
     */
    public function index()
    {
        $meta = [
            'title' => __('messages.news'),
            'desc'  => 'Страница со списком новостей (учебный пример)'
        ];

        // С помощью класса Agent определяем, просматривает ли пользователь текущую страницу с мобильного устройства, и, если да, используем простую пагинацию; в противном случае - обычную
        if (\Agent::isMobile())
            $news = News::orderBy('id', 'desc')->simplePaginate();
        else
            $news = News::orderBy('id', 'desc')->paginate();

        return view('news.index', compact('meta', 'news'))->with('action', __METHOD__);
    }

    /**
     * Отображение формы для создания новости
     *
     * @return Factory|View
     */
    public function create()
    {
        $meta = [
            'title' => __('messages.adding_news')
        ];

        return view('news.create', compact('meta'))->with('action', __METHOD__);
    }

    /**
     * Сохранение новости в базе данных
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $news = new News;

        $news->fill($request->all());
        $news->save();

        return redirect()->route('news.index');
    }

    /**
     * Отображение новости с указанным идентификатором
     *
     * @param int $id
     *
     * @return Factory|View
     */
    public function show($id)
    {
        $news = News::find($id);

        // Просто выбираем первые 2 предложения из новости для тега description
        $temp = explode('.', $news->body);
        if (!empty($temp[1]))
            $desc = $temp[0] . '.' . $temp[1] . '.';
        elseif (!empty($temp[0]))
            $desc = $temp[0] . '.';
        else
            $desc = '';

        $meta = [
            'title' => $news->title,
            'desc'  => $desc
        ];

        return view('news.show', compact('meta', 'news'))->with('action', __METHOD__);
    }

    /**
     * Отображение формы для редактирования заголовка и текста новости с указанным идентификатором
     *
     * @param int $id
     *
     * @return Factory|View
     */
    public function edit($id)
    {
        $news = News::find($id);

        $meta = [
            'title' => __('messages.editing_news')
        ];

        return view('news.edit', compact('meta', 'news'))->with('action', __METHOD__);
    }

    /**
     * Обновление новости с указанным идентификатором
     *
     * @param Request $request
     * @param int     $id
     *
     * @return RedirectResponse
     */
    public function update(Request $request, $id)
    {
        News::whereId($id)->update([
            'title' => $request->get('title'),
            'body'  => $request->get('body')
        ]);

        return redirect()->route('news.show', ['id' => $id]);
    }

    /**
     * Удаление новости с указанным идентификатором
     *
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        News::destroy($id);

        return redirect()->route('news.index');
    }
}
