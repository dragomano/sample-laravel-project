<?php

namespace App\Http\Controllers;

use App\Home;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

/**
 * Class HomeController
 *
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Отображение главной страницы сайта
     *
     * @return Factory|View
     */
    public function index()
    {
        $page = Home::whereAlias('/')->get()[0];

        $meta = [
            'title' => $page->title,
            'desc'  => $page->description
        ];

        return view('home', compact('meta', 'page'))->with('action', __METHOD__);
    }

    /**
     * Отображение формы для редактирования заголовка и текста страницы с указанным идентификатором
     *
     * @param int $id
     *
     * @return Factory|View
     */
    public function edit($id)
    {
        $page = Home::find($id);

        $meta = [
            'title' => $page->title
        ];

        return view('pages.edit', compact('meta', 'page'))->with('action', __METHOD__);
    }

    /**
     * Обновление страницы с указанным идентификатором
     *
     * @param Request $request
     * @param int     $id
     *
     * @return RedirectResponse|Redirector
     */
    public function update(Request $request, $id)
    {
        Home::whereId($id)->update([
            'title'       => $request->get('title'),
            'description' => $request->get('description'),
            'body'        => $request->get('body')
        ]);

        return redirect('/');
    }
}
