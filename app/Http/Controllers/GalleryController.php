<?php

namespace App\Http\Controllers;

use App\Gallery;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Artisan;
use Illuminate\View\View;

/**
 * Class GalleryController
 *
 * @package App\Http\Controllers
 */
class GalleryController extends Controller
{
    // Ширина и высота для ресайза загружаемых изображений
    protected $max_width  = 348;
    protected $max_height = 225;

    /**
     * Отображение всех изображений из таблицы gallery в базе данных
     *
     * @return Factory|View
     */
    public function index()
    {
        $meta = [
            'title' => __('messages.gallery'),
            'desc'  => 'Страница со списком изображений (учебный пример)'
        ];

        // С помощью класса Agent определяем, просматривает ли пользователь текущую страницу с мобильного устройства, и, если да, используем простую пагинацию; в противном случае - обычную
        if (\Agent::isMobile())
            $images = Gallery::orderBy('id', 'desc')->simplePaginate();
        else
            $images = Gallery::orderBy('id', 'desc')->paginate();

        return view('gallery.index', compact('meta', 'images'))->with('action', __METHOD__);
    }

    /**
     * Отображение формы для добавления изображения
     *
     * @return Factory|View
     */
    public function create()
    {
        $meta = [
            'title' => __('messages.adding_media')
        ];

        return view('gallery.create', compact('meta'))->with('action', __METHOD__);
    }

    /**
     * Сохранение информации об изображении в базе данных
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        // Разрешенные типы файлов
        $allowedTypes = array('image/gif', 'image/png', 'image/jpeg');

        foreach ($request->file() as $file) {
            foreach ($file as $f) {
                if (in_array($f->getClientMimeType(), $allowedTypes)) {
                    // Задаем новое имя файлу и копируем в /public/storage
                    $newFilename = $f->hashName();
                    File::copy($f->getRealPath(), public_path('storage/') . $newFilename);

                    // Изменяем изображение в соответствии с желаемыми размерами ($this->$max_width и $this->$max_height)
                    $this->resize($f->getRealPath());

                    // Наконец, перемещаем измененный файл в /public/storage/mini
                    File::copy($f->getRealPath(), public_path('storage/mini/') . $newFilename);

                    // Получаем описание файла (если пользователь указал)
                    $desc = $request->get('description');

                    // Добавляем запись в базу данных
                    $image = new Gallery;
                    $image->filename = $newFilename;
                    $image->description = $desc !== null ?: $f->getClientOriginalName();
                    $image->save();
                }
            }
        }

        return redirect()->route('gallery.index');
    }

    /**
     * Изменение размеров файла-изображения
     *
     * @param $filename
     */
    private function resize($filename)
    {
        $info   = getimagesize($filename);
        $width  = $info[0];
        $height = $info[1];
        $type   = $info[2];

        switch ($type) {
            case 1:
                $img = imageCreateFromGif($filename);
                imageSaveAlpha($img, true);
                break;
            case 2:
                $img = imageCreateFromJpeg($filename);
                break;
            case 3:
                $img = imageCreateFromPng($filename);
                imageSaveAlpha($img, true);
                break;
        }

        $w = $this->max_width;
        $h = $this->max_height;

        if (empty($w)) {
            $w = ceil($h / ($height / $width));
        }
        if (empty($h)) {
            $h = ceil($w / ($width / $height));
        }

        $tmp = imageCreateTrueColor($w, $h);
        if ($type == 1 || $type == 3) {
            imagealphablending($tmp, true);
            imageSaveAlpha($tmp, true);
            $transparent = imagecolorallocatealpha($tmp, 0, 0, 0, 127);
            imagefill($tmp, 0, 0, $transparent);
            imagecolortransparent($tmp, $transparent);
        }

        $tw = ceil($h / ($height / $width));
        $th = ceil($w / ($width / $height));
        if ($tw < $w) {
            imageCopyResampled($tmp, $img, ceil(($w - $tw) / 2), 0, 0, 0, $tw, $h, $width, $height);
        } else {
            imageCopyResampled($tmp, $img, 0, ceil(($h - $th) / 2), 0, 0, $w, $th, $width, $height);
        }

        $img = $tmp;
        $src = $filename;

        switch ($type) {
            case 1:
                imageGif($img, $src);
                break;
            case 2:
                imageJpeg($img, $src, 100);
                break;
            case 3:
                imagePng($img, $src);
                break;
        }

        imagedestroy($img);
    }

    /**
     * Отображение элемента галереи с указанным идентификатором
     *
     * @param int $id
     *
     * @return Factory|View
     */
    public function show($id)
    {
        $image = Gallery::find($id);

        $meta = [
            'title' => $image->description ?: $image->filename,
            'desc'  => $image->description
        ];

        return view('gallery.show', compact('meta', 'image'))->with('action', __METHOD__);
    }

    /**
     * Отображение формы для редактирования элемента галереи с указанным идентификатором
     *
     * @param int $id
     *
     * @return Factory|View
     */
    public function edit($id)
    {
        $image = Gallery::find($id);

        $meta = [
            'title' => __('messages.editing_media')
        ];

        return view('gallery.edit', compact('meta', 'image'))->with('action', __METHOD__);
    }

    /**
     * Обновление элемента галереи с указанным идентификатором
     *
     * @param Request $request
     * @param int     $id
     *
     * @return RedirectResponse
     */
    public function update(Request $request, $id)
    {
        // Разрешенные типы файлов
        $allowedTypes = array('image/gif', 'image/png', 'image/jpeg');

        $filename = $request->get('filename');
        $f = $request->file('image');

        if (!empty($f) && in_array($f->getClientMimeType(), $allowedTypes)) {
            // Используем прежнее имя файла
            File::copy($f->getRealPath(), public_path('storage/') . $filename);

            // Изменяем изображение в соответствии с желаемыми размерами ($this->$max_width и $this->$max_height)
            $this->resize($f->getRealPath());

            // Наконец, перемещаем измененный файл в /public/storage/mini
            $f->move(public_path('storage/mini/'), $filename);
        }

        // Обновляем соответствующую запись в базе данных
        Gallery::whereId($id)->update([
            'filename'    => $filename,
            'description' => $request->get('description')
        ]);

        // Очищаем кэш для обновления загруженной картинки
        Artisan::call('view:clear');

        return redirect()->route('gallery.show', ['id' => $id]);
    }

    /**
     * Удаление файла изображения и записи в таблице gallery с указанным идентификатором
     *
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $image = Gallery::find($id);

        $f = Storage::disk('public');
        $f->delete($image->filename);
        $f->delete('mini/' . $image->filename);

        Gallery::destroy($id);

        return redirect()->route('gallery.index');
    }
}
