<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Libraries\DateFormat;

class News extends Model
{
    protected $fillable = ['title', 'body']; // Доступные для заполнения поля
    protected $perPage  = 3; // Количество новостей на странице

    /**
     * Преобразуем дату создания новости в соответствии с правилами русского языка
     *
     * @param $attr
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|string|null
     */
    public function getCreatedAtAttribute($attr)
    {
        return DateFormat::post($attr);
    }

    /**
     * Преобразуем дату обновления новости в соответствии с правилами русского языка
     *
     * @param $attr
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|string|null
     */
    public function getUpdatedAtAttribute($attr)
    {
        return DateFormat::post($attr);
    }
}
