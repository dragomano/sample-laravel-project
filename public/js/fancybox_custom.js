jQuery(document).ready(function(){
    jQuery(function () {
        jQuery("[data-fancybox=\"gallery\"]").fancybox({
            thumbs: {
                autoStart: true,
                axis: "x"
            }
        });
    });
});