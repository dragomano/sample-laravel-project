<!DOCTYPE html>
<!-- IP: {{Request::getClientIp()}} -->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @if (isset($meta['desc']))
<meta name="description" content="{{$meta['desc']}}">
@endif
    <title>{{ config('app.name', 'Laravel') }} - {{$meta['title']}}</title>

    <!-- Fonts -->
    <link href="//fonts.gstatic.com" rel="dns-prefetch">
    <link href="//fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="//use.fontawesome.com/releases/v5.6.3/css/all.css" rel="stylesheet" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/scrollup.css') }}" rel="stylesheet">
    @if (Request::is('gallery/*') || Request::path() === 'gallery')
<link href="{{ asset('css/album.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery.fancybox.min.css') }}" rel="stylesheet">
@endif
</head>
<body>
    <div id="app">
        @include('layouts.nav')

        <main class="py-4">
            @yield('content')

        </main>
    </div>

    @include('layouts.footer')
</body>
</html>