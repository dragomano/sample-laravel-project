    <a href="#app" id="scrollup"><i class="fas fa-arrow-circle-up"></i></a>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/scrollup.js') }}" defer></script>
@if (Request::is('gallery/*') || Request::path() === 'gallery')
    <script src="{{ asset('js/jquery.fancybox.min.js') }}" defer></script>
@endif
    <!-- {{ $copyright }} -->
