    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link{{ Request::is('news/*') || Request::path() === 'news' ? ' active' : '' }}" href="{{ route('news.index') }}">{{ __('messages.news') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link{{ Request::is('gallery/*') || Request::path() === 'gallery' ? ' active' : '' }}" href="{{ route('gallery.index') }}">{{ __('messages.gallery') }}</a>
                        </li>
                    </ul>
                    <span class="navbar-text">{{ $action }}</span>
                </div>
            </div>
        </nav>