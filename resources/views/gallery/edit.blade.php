@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4 mb-1">
                {{ Form::open(['route' => ['gallery.update', $image->id], 'files' => true]) }}
                <div class="card shadow-sm">
                    @method('PATCH')
                    <div class="card-header">
                        <div class="form-group">
                            {{ Form::label('description', __('messages.media_desc')) }}
                            {{ Form::text('description', $image->description, ['class' => 'form-control']) }}
                            {{ Form::hidden('filename', $image->filename) }}
                        </div>
                        <hr>
                        <div class="form-group">
                            {{ Form::label('image', __('messages.other_file')) }}
                            {{ Form::file('image', ['class' => 'form-control-file', 'accept' => 'image/*']) }}
                        </div>
                    </div>
                </div>
                <div class="card shadow-sm">
                    <div class="card-body" style="padding: 1px">
                        <img class="bd-placeholder-img card-img-top" src="{{ Storage::url('mini/' . $image->filename) }}" alt="{{ $image->description ?: $image->filename }}">
                    </div>
                </div>
                <div class="text-center mt-2">
                    {{ Form::submit(__('messages.save'), ['class' => 'btn btn-primary']) }}
                    <a class="btn btn-secondary" href="{{ route('gallery.show', ['id' => $image->id]) }}" role="button">
                        {{ __('messages.cancel') }}
                    </a>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection