@extends('layouts.app')

@section('content')
    @php
        /** @var \Form */
    @endphp
    <div class="container">
        @include('layouts.errors')
        <div class="row justify-content-center">
            <div class="col-md-8 mb-1">
                {{ Form::open(['route' => 'gallery.store', 'files' => true]) }}
                <div class="card">
                    <div class="card-header">
                        <div class="form-group">
                            {{ Form::label('description', __('messages.media_desc')) }}
                            {{ Form::text('description', null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="card-body">
                        {{ Form::file('image[]', ['class' => 'form-control-file', 'multiple' => true, 'accept' => 'image/*', 'required' => true]) }}
                    </div>
                </div>
                <div class="text-center mt-2">
                    {{ Form::submit(__('messages.save'), ['class' => 'btn btn-primary']) }}
                    <a class="btn btn-secondary" href="{{ route('gallery.index') }}" role="button">{{ __('messages.cancel') }}</a>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection