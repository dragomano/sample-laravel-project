@extends('layouts.app')

@section('content')
    <section class="jumbotron text-center" style="background: none">
        <div class="container">
            <h1 class="jumbotron-heading">Пример галереи</h1>
            <p class="lead text-muted">Обычный список изображений, по 9 шт. на странице.</p>
            <p>
                <a href="{{ route('gallery.create') }}" class="btn btn-primary my-2">{{ __('messages.add_media') }}</a>
            </p>
        </div>
    </section>
    @if (!empty($images[0]))
    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                @foreach ($images as $item)
                <div class="col-md-4">
                    <div class="card mb-4 shadow-sm"><a href="{{ Storage::url('/' . $item->filename) }}" data-fancybox="gallery" data-caption="{{ $item->description ?: $item->filename }}">
                            <img class="bd-placeholder-img card-img-top" src="{{ Storage::url('mini/' . $item->filename) }}" alt="{{ $item->description ?: $item->filename }}"></a>
                        <div class="card-body">
                            <p class="card-text">{{ $item->description }}</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <a href="{{ route('gallery.show', ['id' => $item->id]) }}" class="btn btn-sm btn-outline-secondary" role="button">{{ __('messages.view') }}</a>
                                    <a href="{{ route('gallery.edit', ['id' => $item->id]) }}" class="btn btn-sm btn-outline-secondary" role="button">{{ __('messages.edit') }}</a>
                                </div>
                                <small class="text-muted">{{ $item->created_at }}</small>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="row justify-content-center">
                {{ $images->links() }}
            </div>
        </div>
    </div>
    @endif
@endsection