@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 mb-1">
                {{ Form::model($image, ['route' => ['gallery.update', $image->id]]) }}
                <div class="card">
                    @method('DELETE')
                    <div class="card-header text-center">{{ $image->filename }}</div>
                    <div class="card-body" style="padding: 1px">
                        <img class="bd-placeholder-img card-img-top" src="{{ Storage::url($image->filename) }}" alt="{{ $image->description ?: $image->filename }}">
                    </div>
                    @if ($image->description)
                    <div class="card-body text-center">
                        {{ $image->description }}
                    </div>
                    @endif
                    <div class="card-footer text-muted">
                        {{ __('messages.created') }}{{ $image->created_at }}
                        @if ($image->updated_at > $image->created_at)
                        <span class="float-right">
                            {{ __('messages.updated') }}{{ $image->updated_at }}
                        </span>
                        @endif
                    </div>
                </div>
                <div class="text-center mt-2">
                    <a class="btn btn-secondary" href="{{ route('gallery.edit', ['id' => $image->id]) }}" role="button">
                        {{ __('messages.edit') }}
                    </a>
                    {{ Form::submit(__('messages.remove'), ['class' => 'btn btn-danger']) }}
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection