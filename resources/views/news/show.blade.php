@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 mb-1">
                {{ Form::model($news, ['route' => ['news.update', $news->id]]) }}
                <div class="card">
                    @method('DELETE')
                    <div class="card-header">{{ $news->title }}</div>
                    <div class="card-body">
                        {{ $news->body }}
                    </div>
                    <div class="card-footer text-muted">
                        {{ __('messages.created') }}{{ $news->created_at }}
                        @if ($news->updated_at > $news->created_at)
                        <span class="float-right">
                            {{ __('messages.updated') }}{{ $news->updated_at }}
                        </span>
                        @endif
                    </div>
                </div>
                <div class="text-center mt-2">
                    <a class="btn btn-secondary" href="{{ route('news.edit', ['id' => $news->id]) }}" role="button">
                        {{ __('messages.edit') }}
                    </a>
                    {{ Form::submit(__('messages.remove'), ['class' => 'btn btn-danger']) }}
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection