@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center mb-2">
            <a class="btn btn-primary" href="{{ route('news.create') }}" role="button">{{ __('messages.add_news') }}</a>
        </div>
        <div class="row justify-content-center">
        @foreach ($news as $item)
            <div class="col-md-8 mb-1">
                <div class="card shadow-sm bg-white rounded">
                    <div class="card-header">
                        <a href="{{ route('news.show', ['id' => $item->id]) }}">{{ $item->title }}</a>
                    </div>
                    <div class="card-body">
                        {{ $item->body }}
                    </div>
                    <div class="card-footer text-muted">{{ $item->created_at }}</div>
                </div>
            </div>
        @endforeach
        </div>
        <div class="row justify-content-center">
            {{ $news->links() }}
        </div>
    </div>
@endsection