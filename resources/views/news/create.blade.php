@extends('layouts.app')

@section('content')
    @php
        /** @var \Form */
    @endphp
    <div class="container">
        @include('layouts.errors')
        <div class="row justify-content-center">
            <div class="col-md-8 mb-1">
                {{ Form::open(['route' => 'news.store']) }}
                <div class="card">
                    <div class="card-header">
                        <div class="form-group">
                            {{ Form::label('title', __('messages.title')) }}
                            {{ Form::text('title', null, ['class' => 'form-control', 'required' => true]) }}
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            {{ Form::label('body', __('messages.news_body')) }}
                            {{ Form::textarea('body', null, ['class' => 'form-control', 'required' => true]) }}
                        </div>
                    </div>
                </div>
                <div class="text-center mt-2">
                    {{ Form::submit(__('messages.save'), ['class' => 'btn btn-primary']) }}
                    <a class="btn btn-secondary" href="{{ route('news.index') }}" role="button">{{ __('messages.cancel') }}</a>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection