@extends('layouts.app')

@section('content')
    @php
        /** @var \Form */
    @endphp
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 mb-1">
                {{ Form::model($news, ['route' => ['news.update', $news->id]]) }}
                <div class="card">
                    @method('PATCH')
                    <div class="card-header">
                        <div class="form-group">
                        {{ Form::label('title', 'Заголовок') }}
                        {{ Form::text('title', $news->title, ['class' => 'form-control', 'required' => true]) }}
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                        {{ Form::label('body', 'Текст новости') }}
                        {{ Form::textarea('body', $news->body, ['class' => 'form-control', 'required' => true]) }}
                        </div>
                    </div>
                    <div class="card-footer text-muted">
                        {{ __('messages.created') }}{{ $news->created_at }}
                        @if ($news->updated_at > $news->created_at)
                            <span class="float-right">
                            {{ __('messages.updated') }}{{ $news->updated_at }}
                        </span>
                        @endif
                    </div>
                </div>
                <div class="text-center mt-2">
                    {{ Form::submit(__('messages.save'), ['class' => 'btn btn-primary']) }}
                    <a class="btn btn-secondary" href="{{ route('news.show', ['id' => $news->id]) }}" role="button">
                        {{ __('messages.cancel') }}
                    </a>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection