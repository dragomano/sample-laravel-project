<?php

return [
    'later'             => ':date в :time',
    'today'             => 'сегодня в :time',
    'yesterday'         => 'вчера в :time',
    'month_declensions' => [
        'January'   => 'января',
        'February'  => 'февраля',
        'March'     => 'марта',
        'April'     => 'апреля',
        'May'       => 'мая',
        'June'      => 'июня',
        'July'      => 'июля',
        'August'    => 'августа',
        'September' => 'сентября',
        'October'   => 'октября',
        'November'  => 'ноября',
        'December'  => 'декабря'
    ]
];