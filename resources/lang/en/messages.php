<?php

return [
    'welcome'       => 'Welcome!',
    'news'          => 'News',
    'gallery'       => 'Gallery',
    'created'       => 'Created: ',
    'updated'       => 'Updated: ',
    'save'          => 'Save',
    'add_news'      => 'Add news',
    'add_media'     => 'Add item to Gallery',
    'view'          => 'View',
    'edit'          => 'Edit',
    'remove'        => 'Delete',
    'cancel'        => 'Cancel',
    'adding_news'   => 'Adding news',
    'editing_news'  => 'Editing news',
    'adding_media'  => 'Adding item to Gallery',
    'editing_media' => 'Editing item',
    'title'         => 'Title',
    'alias'         => 'Alias',
    'desc'          => 'Description',
    'body'          => 'Page text',
    'media_desc'    => 'Image description',
    'news_body'     => 'News text',
    'other_file'    => 'Select other file to upload'
];